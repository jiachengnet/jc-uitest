import os
import sys
import logging

PRO_PATH = os.path.dirname(os.path.abspath(__file__))


class RunConfig:
    """
    运行测试配置
    """

    # 运行单个模块用例
    if sys.argv[1] != "testCases":
        cases_path = os.path.join(PRO_PATH, "testCases", sys.argv[1])
    else:

    # 运行所有模块用例
        cases_path = os.path.join(PRO_PATH, "testCases")

    # 配置浏览器驱动类型(chromium, firefox, webkit)。
    browser = "chromium"

    # 运行模式（headless, headed）
    mode = "headed"

    # 失败重跑次数
    rerun = "0"

    # 当达到最大失败数，停止执行
    max_fail = "5"

    # 报告路径（不需要修改）
    NEW_REPORT = None

    # 项目目录配置
    BASE_DIR = os.path.dirname(os.path.abspath(__file__))
    REPORT_DIR = BASE_DIR + "/report/"

    # 是否开启企业微信通知，开启企业微信通知时，设置为TRUE；否则，则不开启企业微信通知
    notice_status=True

    # 企业微信机器人地址
    chatbot_url=""

    # 产品名称及版本号
    product_name = ""

    # 项目名(gitlab项目名称)
    project_name = ""

    # 测试报告地址（测试服务器ip地址+webserver端口号）
    server_address = ""

    # 若命令行中传入了mode、server_address，使用外部参数替换配置文件中的参数；若不传入以下命令，则使用config.py中默认配置
    try:
        # 从第二个命令行参数开始遍历“：”
        print(sys.argv.__len__())
        for num in range(2, sys.argv.__len__()):
            if ":" in sys.argv[num]:
                if sys.argv[num].split(":")[0].strip() == "mode":
                    mode = sys.argv[num].replace("mode:", "").strip()

                elif sys.argv[num].split(":")[0].strip() == "server_address":
                    server_address = sys.argv[num].replace("server_address:", "").strip()

                elif sys.argv[num].split(":")[0].strip() == "notice_status":
                    notice_status = sys.argv[num].replace("notice_status:", "").strip()

                else:
                    raise Exception()
            else:
                raise Exception()
    except Exception as e:
        logging.error("传入参数格式错误",e)
        sys.exit()

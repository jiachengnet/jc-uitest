# author: 宿爽
# date: 2021/11/13
# email: sushuang@jiachengnet.com

import json

def get_data(json_path,casename):
    """
    读取json文件
    :return:
    """
    dataList = []
    with open(json_path,encoding="utf-8") as f:
        dict_data = json.loads(f.read())[casename]
        for i in dict_data:
            dataList.append(tuple(i.values()))
    return dataList

